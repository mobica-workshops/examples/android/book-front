package com.mobica.booker

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.mobica.booker.databinding.FragmentSecondBinding
import okhttp3.Call
import okhttp3.Callback
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException

data class ImageInfo (var imageUrl: String, var imageBase64: String)
data class BookOutput(var author: String, var title: String, var description: String, var year: String,
                var image: ImageInfo)

data class SingleBookResponse(var content: Book)

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null
    private var actionType: String? = null
    private var bookId: Int? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        actionType = arguments?.getString("actionType")
        bookId = arguments?.getInt("bookId")
        binding.cancel.setOnClickListener { findNavController().popBackStack() }
        binding.save.setOnClickListener { handleSave() }

        if(actionType.equals("CREATE")){
            // hide edit button
            binding.edit.isVisible = false
            binding.author.isEnabled = true
            binding.title.isEnabled = true
            binding.description.isEnabled = true
            binding.year.isEnabled = true
            binding.save.isEnabled = true

        }
        else{
            getBookById()
            binding.edit.setOnClickListener { enableEdit() }
        }
    }

    private fun handleSave(){
        if(actionType.equals("CREATE")){
            createBook()
        }
        else{
            updateBook()
        }
    }

    private fun updateBook() {
        val bookOutput = BookOutput(binding.author.text.toString(), binding.title.text.toString(),
            binding.description.text.toString(), binding.year.text.toString(), ImageInfo("a", "a"))
        val requestBody = Gson().toJson(bookOutput)
        val mediaTypeJson = "application/json; charset=utf-8".toMediaType()//4.0.1

        val request = Request.Builder()
            .url("http://10.0.2.2:8080/v1/books/$bookId")
            .put(requestBody.toRequestBody(mediaTypeJson))
            .build()

        OkHttpClient().newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                println(e.message)
            }

            override fun onResponse(call: Call, response: okhttp3.Response) {
                println(response)
                requireActivity().runOnUiThread { findNavController().popBackStack() }

            }
        })

    }

    private fun enableEdit() {
        actionType = "EDIT"
        binding.edit.isVisible = false
        binding.author.isEnabled = true
        binding.title.isEnabled = true
        binding.description.isEnabled = true
        binding.year.isEnabled = true
        binding.save.isEnabled = true
    }

    private fun createBook(){
        val bookOutput = BookOutput(binding.author.text.toString(), binding.title.text.toString(),
            binding.description.text.toString(), binding.year.text.toString(), ImageInfo("a", "a"))
        val requestBody = Gson().toJson(bookOutput)
        val mediaTypeJson = "application/json; charset=utf-8".toMediaType()//4.0.1

        val request = Request.Builder()
            .url("http://10.0.2.2:8080/v1/books")
            .post(requestBody.toRequestBody(mediaTypeJson))
            .build()

        OkHttpClient().newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                println(e.message)
            }

            override fun onResponse(call: Call, response: okhttp3.Response) {
                println(response)
                requireActivity().runOnUiThread { findNavController().popBackStack() }

            }
        }
        )
    }
    private fun getBookById(){
        println(bookId)
        var resAsJson: SingleBookResponse?
        val request = Request.Builder()
            .url("http://10.0.2.2:8080/v1/books/$bookId")
            .build()

        OkHttpClient().newCall(request).enqueue(object :Callback{
            override fun onFailure(call: Call, e: IOException) {
            }

            override fun onResponse(call: Call, response: okhttp3.Response) {
                val responseAsString = response.body!!.string()
                println(responseAsString)
                resAsJson = Gson().fromJson(responseAsString, SingleBookResponse::class.java)
                requireActivity().runOnUiThread {
                    binding.title.setText(resAsJson?.content?.title)
                    binding.author.setText(resAsJson?.content?.author)
                    binding.description.setText(resAsJson?.content?.description)
                    binding.year.setText(resAsJson?.content?.year)
                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}