package com.mobica.booker

import android.content.Context
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

class CustomAdapter(private val context: Context?, private val dataSet: List<Book>, private val bookItemEventListener: BookItemEventListener) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder)
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val author: TextView
        val title: TextView
        val year: TextView
        val deleteBtn: Button
        val coverImage: ImageView

        init {
            author = view.findViewById(R.id.author)
            title = view.findViewById(R.id.title)
            year = view.findViewById(R.id.year)
            deleteBtn = view.findViewById(R.id.delete)
            coverImage = view.findViewById(R.id.coverImage)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.text_row_item, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.setOnClickListener {
            // Handle item click
            Log.d("RP", "clicked")
            bookItemEventListener.onBookItemClicked(dataSet[position].id)
        }

        viewHolder.deleteBtn.setOnClickListener {
            // Handle item click
            Log.d("RP", "clicked delete$dataSet[position].id")
            bookItemEventListener.onBookItemDeleteClicked(dataSet[position].id)
        }
        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.author.text = dataSet[position].author
        viewHolder.title.text = dataSet[position].title
        viewHolder.year.text = dataSet[position].year

        var imageSource: String? = null
        if(dataSet[position].imageUrl?.isNotEmpty() == true){
            imageSource = dataSet[position].imageUrl
        }
        else if(dataSet[position].imageBase64?.isNotEmpty() == true){
            imageSource = dataSet[position].imageBase64
        }

        if(imageSource != null){
            val requestOptions = RequestOptions().transforms(FitCenter(), RoundedCorners(16))
            Glide.with(context!!)
                .load(imageSource)
                .apply(requestOptions)
                .skipMemoryCache(true)//for caching the image url in case phone is offline
                .into(viewHolder.coverImage)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

}
