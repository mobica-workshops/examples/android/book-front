package com.mobica.booker

data class Book(var id: Int, var author: String, var title: String, var description: String, var year: String?,
                var imageUrl: String?, var imageBase64: String?)
