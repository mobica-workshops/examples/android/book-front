package com.mobica.booker

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.mobica.booker.databinding.FragmentFirstBinding
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */

data class Response (var status: String, var content: Content)

data class Content(var results: List<Book>)

class FirstFragment : Fragment(), BookItemEventListener,
     BookDeleteListener {

    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.create.setOnClickListener {
            val bundle = bundleOf("actionType" to "CREATE")
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment, bundle)
        }

    }

    override fun onResume() {
        super.onResume()
        run(this)
    }

    private fun run(bookItemEventListener: BookItemEventListener) {
        var resAsJson: Response?
            val request = Request.Builder()
                .url("http://10.0.2.2:8080/v1/books")
                .build()

        OkHttpClient().newCall(request).enqueue(object :Callback{
            override fun onFailure(call: Call, e: IOException) {
            }

            override fun onResponse(call: Call, response: okhttp3.Response) {
                println(response)
                val responseAsString = response.body!!.string()
                resAsJson = Gson().fromJson(responseAsString, Response::class.java)
                requireActivity().runOnUiThread(){
                    binding.books.adapter = CustomAdapter(context, resAsJson!!.content.results,
                        bookItemEventListener)
                    }
                }
        })

    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onBookItemClicked(id: Int) {
        val bundle = bundleOf("actionType" to "VIEW", "bookId" to id)
        findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment, bundle)
    }

    override fun onBookItemDeleteClicked(id: Int) {
        DeleteDialogFragment(this, id).show(
            childFragmentManager, DeleteDialogFragment.TAG)
    }


    override fun onBookDeleted() {
        println("bookdeleted")
        run(this)
    }
}