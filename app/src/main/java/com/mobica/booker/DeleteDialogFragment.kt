package com.mobica.booker

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException

interface BookDeleteListener{
    fun onBookDeleted()
}
class DeleteDialogFragment(private val bookDeleteListener: BookDeleteListener, private val bookId: Int) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        AlertDialog.Builder(requireContext())
            .setMessage("Are you sure to delete this book?" )
            .setPositiveButton(getString(android.R.string.ok)) { _, _ -> deleteBook()}
            .setNegativeButton(getString(android.R.string.cancel)) { _, _ -> }
            .create()


    private fun deleteBook(){
        val request = Request.Builder()
            .url("http://10.0.2.2:8080/v1/books/$bookId")
            .delete()
            .build()

        OkHttpClient().newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                println("e.message")
            }

            override fun onResponse(call: Call, response: okhttp3.Response) {
                bookDeleteListener.onBookDeleted()
                println(response)
                }
            }
        )
    }

    companion object {
        const val TAG = "PurchaseConfirmationDialog"
    }
}
