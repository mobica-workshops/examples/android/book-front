package com.mobica.booker

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

interface BookItemEventListener{
    fun onBookItemClicked(id: Int)
    fun onBookItemDeleteClicked(id: Int)
}

// val booksList = arrayOf(Book("author", "title", "description", "2023", "", ""),
// Book("author2", "title2", "description2", "2022", "", ""))

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}
